#ifndef __TOOLS_H__
#define __TOOLS_H__
#include <iostream>
#include "sys/types.h"
#include "boost/regex.hpp"
#include "boost/format.hpp"   
#include "boost/tokenizer.hpp"   
#include "boost/algorithm/string.hpp"
using namespace std;
using namespace boost;

regex expression("^SELECT *([a-z_A-Z0-9 ,]*) *FROM *([a-z_A-Z0-9 ,]*) *WHERE *([a-z_A-Z0-9 ,<>=']*) *;? *");
regex noWhereExp("^SELECT *([a-z_A-Z0-9 ,]*) *FROM *([a-z_A-Z0-9 ,]*) *;? *"); 

boost::char_separator<char> comma(",");
boost::char_separator<char> space(" ");
boost::char_separator<char> cmsp(", ");
void split_word(const string& strTag,vector<string>& vecSegTag,boost::char_separator<char>& sep){
	typedef boost::tokenizer<boost::char_separator<char> >  
		CustonTokenizer;  
	CustonTokenizer tok(strTag,sep);  

	// 输出分割结果   
	for(CustonTokenizer::iterator beg=tok.begin(); beg!=tok.end();++beg)  
	{  
		vecSegTag.push_back(*beg);  
	}  


}


void parseSQL(string& in,vector<string>& selectCol,vector<string>& fromCol,vector<string>& whereCol){

	
	cmatch what;
	if(regex_match(in.c_str(), what, expression))

	{
		//不懂为什么2没有。。。
		vector<string> tmp;
		split_word(what[1].str(),selectCol,cmsp);
		split_word(what[2].str(),fromCol,cmsp);
		split_word(what[3].str(),whereCol,cmsp);


	}

	else if (regex_match(in.c_str(),what,noWhereExp)){
		split_word(what[1].str(),selectCol,cmsp);
		split_word(what[2].str(),fromCol,cmsp);
	}

	else
	{

		cout<<"Error Input"<<endl;

	}
}
#endif
