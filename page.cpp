#include "page.h"
#include "list.h"
#include "table.h"

int page::NowTotP = 0;
UC page::retbuffer[pageLength];
UC page::strbuffer[pageLength];
UC page::tmpbuffer[pageLength];

void outStr(char* p,int k = 1){
	while(k--){
		while(*p){
			//cout<<int(*p)<<" ";
			p++;
		}
		p++;
	}
	putchar('\n');
}
listnode* page::getPage(){
	if (ListPos!=NULL){
		List::shift(ListPos);
		return ListPos;
	}
	//cout<<"getpage "<<"  "<<Text<<endl;

	//cout<<parent->numS<<" "<<parent->strS<<endl;
	return (ListPos = List::renew(Text,this,parent->numS,parent->strS,parent->rowPerPage));
}



void page::ReWritePage(listnode * ln){
}


page::page(table* p,int text,page* pre,page* next){
	parent = p;
	Text = text;
	Before =pre;
	After = next;
	ListPos = NULL;
	rowS = 0;

}

page::page(table* p,int tt,page* pre,page* next,vector<string>& rows){
	parent = p;
	Text = tt;
	rowS = rows.size();
	//cout<<"page::rowS "<<rowS<<endl;
	//	//cout<<parent->numS<<' '<<parent->strS<<' '<<tt<<' '<<rowS<<endl;

	Before = pre;
	After = next;
	int len = parse(rows);
	writeToDisk(retbuffer,len);
	ListPos = NULL;

}

void page::add(UC*& a,UC*& b,int k ){

	while (k--){
		while(*b){
			*a = *b;
			a++;b++;
			//len++;
		}
		b++;
		*(a++) = 0;
		//len++;
	}
}
void page::add(UC*& a,const string& s){
	int l = s.size();
	for (int i = 0 ;i < l ;i ++){
		*a = s[i];
		a++;
	}
	*(a++) = '\0';
}


void page::add(UC*& a, int b){
	int k = 4;
	while(k--){
		*a = b&0xff;
		a++;
		b>>=8;
	}
	//len+=4;
}

int page::parse(vector<string>& rows){
	UC* p1 = retbuffer;
	UC* p2 = strbuffer;
	UC* tmp = tmpbuffer;
	//len = 0;

	bool isNum = true;
	foreach(e,rows,vector<string>){
		string& s = (*e);

		int l = s.size();
		p2 = strbuffer;
		for(int i = 0 ;i <= l ;i ++){
			////cout<<"cur is "<<i <<" "<<s[i]<<endl;
			if (i==l||s[i]==','){

				*tmp = 0;
				if(isNum){
					tmp = tmpbuffer;
					int	d = 0;
					while(*tmp){
						d = d*10+(*tmp)-'0';
						tmp++;
					}
					//cout<<"d = "<<hex<<d<<endl;
					add(p1,d);
				}
				else{
					*(tmp-1) = 0;
					tmp = tmpbuffer+1;
					add(p2,tmp);
				}

				tmp = tmpbuffer;

				isNum = true;
			}

			else{
				*tmp = s[i];
				tmp++;
				if(('0'>s[i])||('9'<s[i])){
					isNum = false;
				}
			}
		}


		p2 = strbuffer;
		add(p1,p2,parent->strS);

	}

	len = p1 - retbuffer;
	return len;
}
int page::parse(listnode* ln,vector<bool>& colType){
	UC* p1 = retbuffer;
	bool isNum = true;
	int pi = 0, ps = 0;
	int len;
	int l = ln->rowS;

	for (int i = 0 ;i < l; i++){
		for (int j = 0 ; j < parent->numS;j++)
			add(p1,ln->I[j][i]);
		for (int j = 0 ; j < parent->strS;j++)
			add(p1,ln->S[j][i]);
	}
	len = p1 - retbuffer;
	return len;
}


void page::writeToDisk(UC* buffer,size_t len,bool toAppend){
	char address[20];
	//cout<<"writing to "<<Text<<endl;
	sprintf(address,"%s%d.page",List::dataFolder,Text);
	FILE *pFile;
	if (toAppend){
		pFile = fopen(address,"ab+");
	}
	else {
		pFile = fopen(address,"wb");
	}
	fwrite(buffer,1,len,pFile);

	fclose(pFile);
	//cout<<"writed"<<endl;
}

page::~page(){
	if(ListPos){
		List::remove(ListPos);
	}
	//cout<<"hehe"<<endl;
}
void page::insert(const string& s){
	int l = s.size();
	UC* p1 = retbuffer;
	UC* p2 = strbuffer;
	UC* tmp = tmpbuffer;
	p2 = strbuffer;
	bool isNum = true;
	for (int i = 0;i<=l;i++){
		if (i==l||s[i]==','){
			*tmp = 0;
			if(isNum){
				tmp = tmpbuffer;
				int d = 0;
				while(*tmp){
					d = d*10+(*tmp)-'0';
					tmp++;
				}
				//cout<<"d = "<<hex<<d<<endl;
				add(p1,d);
			}
			else{
				*(tmp-1)= 0;
				add(p2,tmpbuffer+1);
			}

			tmp = tmpbuffer;
			isNum = true;
		}
		else{
			*tmp = s[i];
			tmp++;
			if(('0'>s[i])||('9'<s[i])){
				isNum = false;
			}
		}
	}
	rowS++;
	p2 = strbuffer;
	add(p1,p2,parent->strS);
	len = p1 - retbuffer;
	writeToDisk(retbuffer,len,true);

}
void page::writeToDisk(listnode* ln, vector<bool>& colType){
	int len = parse(ln,colType);
	rowS = ln->rowS;
	writeToDisk(retbuffer,len);
}

void  page::getCSV(const vector<string>& ColUsed,vector<string>& result){
	//cout<<"<csv>"<<endl;
	//暂时500吧
	char tmpStr[5000];
	char rowStr[5000];
	char* p = rowStr;
	table* tb = parent;

	int cl = ColUsed.size();

	int *mapping = new int[cl];
	memset(mapping,-1,4*cl);
	int iN=0,sN=0;
	//现在用弱的方法
	vector<int> s;
	for (int i = 0;i<cl;i++){
		if(parent->ColType[i]){
			s.push_back(sN++);
		}else
			s.push_back(iN++);

	}

	for (int i = 0;i<cl;i++){
		for (int j = 0 ;j <cl ; j++){

			if (parent->ColName[j]==ColUsed[i]){
				mapping[i] = j ;
				break;
			}
		}
	}


	listnode* cur = getPage();
	int l = cur->rowS;
	for (int i = 0;i<l;i++){
		p = rowStr;
		for (int k = 0;k<cl;k++){
			int j = mapping[k];

			if(tb->ColType[j]==0){
				sprintf(tmpStr,"%d",cur->I[s[j]][i]);
				add(p,tmpStr);
				//因为默认会在末尾加一个'\0'，这里直接覆盖成','
				*(p-1) = ',';
			}
			else{
				*(p++) = 39;
				add(p,cur->S[s[j]][i]);
				*(p-1) = 39;
				*(p++) = ',';
			}
		}
		*(p-1)=0;
		result.push_back(rowStr);
	}
	delete[] mapping;
	//cout<<"</csv>"<<endl;
}




int page::getLastRow(){
	if(ListPos){
		return ListPos->rowS;
	}
	return rowS;
}
