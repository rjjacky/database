
#include "db.h"

int db::NowT = 0;

db::db(){
	List::init();
}
db::~db(){
	foreach(e,OriginalTable,vector<table*>){
		delete *e;
	}
	List::clean();
}

string db::newT(const vector<string>& ColList){
	int cl = ColList.size();
	vector<bool> colType;
	int l = ColList.size();
	int iN = 0,sN = 0;
	int len = 0;
	for (int i = 0 ;i <l;i++){
		//cout<<i<<" "<<ColList[i]<<endl;
		int s = ColumnType[ColList[i]];
		//cout<<"columnType "<<s<<endl;
		if(s>0){
			sN++;
			colType.push_back(true);
			len += s+1;
			
		}
		else {
			iN++;
			colType.push_back(false);
			len +=4;
		}

	}

	char tmp[20];
	//TODO: 感觉上NowT应该是连续的,这样就不需要这个vector了
	tempTable.push_back(NowT);
	sprintf(tmp,"RYT%d",NowT++);
	string name = tmp;
	//cout<<"name " <<name<<endl;
	table *newTable = new table(iN,sN,len,this,colType,ColList);
	newTable->getNewPage();
	//cout<<"newTable "<<newTable<<endl;

	TableMap.insert(pst(name,newTable));
	return name;
}
table* db::getTableByName(string s){
	return TableMap[s];
}

table* db::getTableByColumn(const string& s){
	return ColumnMap[s];
}

int db::getColumnType(string s){
	return ColumnType[s];
}

int db::getColumnNumber(const string& s){
	return ColumnNumMap[s];
}

table* db::InsertT(const string &Name,const vector<string>& Col, const vector<string>& Type){
	int sN = 0,iN = 0;
	int ret  ;
	int len=0;
	int l = Col.size();
	vector<bool> colType;


	for (int i = 0 ;i < l ; i++){
		const string& c = Col[i];
		const string& t = Type[i];
		bool tp;
		if (t[0]=='I'){
			tp = false;
			ColumnType.insert(psi(c,0));
			ColumnNumMap.insert(psi(c,iN));
			//cout<<"Insert "<<c<<" "<<iN<<"~~~~~~~~"<<getColumnNumber(c)<<endl;
			iN++;
		}
		else{
			tp = true;
			ret = 0;
			for(int j = 8;t[j]!=')';j++){
				ret = ret*10+t[j]-'0';
			}
			len += ret+1;
			ColumnType.insert(psi(c,ret));
			ColumnNumMap.insert(psi(c,sN));
			sN++;
		}
		colType.push_back(tp);
		//psi: see define in db.h
		
	}	
	len += iN*4;
	table* newTb = new table(iN,sN,len,this,colType,Col);
	OriginalTable.push_back(newTb);
	for(int i = 0 ;i < l;i++){
		ColumnMap.insert(pst(Col[i],newTb));
	}

	TableMap.insert(pst(Name,newTb));
	
		
	return newTb;
}


void db::FixDb(){
	//cout<<"fixdb"<<endl;

	map<string,table*>::iterator  iter;
	string name;
	char tmp[20];
	foreach(e,tempTable,vector<int>){
		sprintf(tmp,"RYT%d",*e);
		name = tmp;
		//cout<<name<<endl;
		iter = TableMap.find(name);
		//cout<<"p2"<<endl;
		//cout<<iter->second<<endl;
		delete iter->second;
		//cout<<"p3"<<endl;
		TableMap.erase(iter);
	}
	tempTable.clear();
	//cout<<"p1"<<endl;

	vector<Judge*> judgeTmp;
	foreach(e,OriginalTable,vector<table*>){
		(*e)->reDb(judgeTmp);
	}

}


