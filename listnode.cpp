
#include "listnode.h"

char listnode::buffer[pageLength];
listnode::listnode(page* p,UC* data,UC* end,int iN,int sN,int rL){
	setPage(p);
	rowS = 0;
	strS = sN;
	numS = iN;
	rowLimit = rL;
	updated = false;
	Before = After = NULL;
	
	S = new string*[sN];
	I = new int*[iN];
	for (int i = 0;i<sN;i++){
		S[i] = new string[rowLimit];
	}
	for (int i = 0;i<iN;i++){
		I[i] = new int[rowLimit];
	}
	if (data){
		parse(iN,sN,data,end);
	}
}
void listnode::parse(int iN,int sN,UC *data,UC *end){
	
	UC bufferstr[pageLength];
	UC tmpbuffer[pageLength];
	int k;
	while(data!=end&&(rowS<rowLimit)){
		for( int j = 0 ; j < iN; j ++){
			k = (*data)|((*(data+1))<<8)|((*(data+2))<<16)|((*(data+3))<<24);
			k = (*(data+3))&0xff;
			k = (k<<8)|((*(data+2))&0xff);
			k = (k<<8)|((*(data+1))&0xff);
			k = (k<<8)|((*data)&0xff);
			I[j][rowS] = k;
			data+=4;
		}
		for (int j = 0 ;j < sN;j++){

			UC* buffer = bufferstr;
			while(*data!=0){
				*(buffer++) = *(data++);
			}
			data++;
			*(buffer++) = 0;
			
			//TODO:先用猥琐方法实现，后面要优化

			int l = buffer - bufferstr;


			char *p = tmpbuffer;	
			
			buffer = bufferstr;	
			while(*buffer){
				*(p++)=*(buffer++);
			}
			*p=0;
//			S[j].push_back(tmpbuffer);
			S[j][rowS] = tmpbuffer;
		}
		rowS++;
	}

	//cout<<"</listnode::parse>"<<endl;
}

void listnode::setPage(page* p){
	PagePos = p;
}



listnode::~listnode(){

	if (S){
		for (int i = 0 ;i < strS;i++){
			delete[] S[i];
		}
		delete[] S;
	}
	if (I){
		for (int i = 0 ;i < numS;i++){
			delete[] I[i];
		}
		delete[] I;
	}

}

void listnode::insert(const string& s){
	
	updated = true;
	char* tmp = buffer;
	int l = s.size();
	bool isNum = true;
	int ci=0,cs=0;
	for (int i = 0;i <=l; i++){
		if (i==l||s[i]==','){
			*(tmp) = '\0';
			if(isNum){
				int d = 0;
				tmp = buffer;
				while(*tmp){
					d = d*10+(*tmp)-'0';
					tmp++;
				}
				I[ci][rowS]=(d);
				ci++;
			}
			else{
				*(tmp-1)= 0;
				S[cs][rowS]=buffer+1;
				cs++;
			}
			tmp = buffer;
			isNum = true;
		}
		else{
			*(tmp++) = s[i];
			if (('0'>s[i])||('9'<s[i])){
				isNum = false;
			}
		}
	}
	rowS++;

}
				



int listnode::LastRow(){
	return rowS;
}
