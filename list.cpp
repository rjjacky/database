
#include "list.h"
#include "page.h"
#include "table.h"

listnode* List::First ;
listnode* List::Last ;
int List::NowTotal = 0;
int List::sizeLimit = 4;
char List::dataFolder[10]="data/" ;
UC List::buffer[pageLength];
	
void List::init(){
	First = new listnode(NULL);
	Last = new listnode(NULL);
	First->After = Last;
	Last->Before = First;
}


listnode* List::renew(int T,page* p,int intN,int strN,int rowNeeded){
	char address[20];
	sprintf(address,"%s%d.page",dataFolder,T);
	FILE* pFile = fopen(address,"rb");
	fseek (pFile , 0 , SEEK_END);
	long lSize = ftell (pFile);
	rewind (pFile);
	if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}
	size_t result = fread (buffer,1,lSize,pFile);
	if (result != lSize) {fputs ("Reading error",stderr); exit (3);}
	fclose(pFile);
	buffer[lSize] = 127;
	listnode* ret=new listnode(p,buffer,buffer+lSize,intN,strN,rowNeeded);
	insert(ret);
	return ret;
}


void List::clean(){
	listnode* cur = First;
	while(cur){
		listnode* p = cur;
		cur = cur->After;
		delete p;
	}
}
void List::remove(listnode* cur){
	listnode* pre = cur->Before;
	listnode* aft = cur->After;
	pre -> After = aft;
	aft -> Before = pre;

	delete cur;
	NowTotal--;
}


void List::insert(listnode* ret){
	ret->Before = Last->Before;
	ret->After = Last;
	Last->Before->After = ret;
	Last->Before = ret;
	if (NowTotal >= sizeLimit){
		listnode* cur = First->After;
		cur->After->Before = First;
		First->After = cur->After;
		
		page* pg = cur->PagePos;
		pg->parent->renewPage(cur,pg);
		pg->ListPos = NULL;
		
		delete cur;
		NowTotal--;
	}
	NowTotal++;
}


void List::shift(listnode* cur){
	if (cur->After == Last) return;
	cur->Before -> After = cur->After;
	cur->After->Before = cur->Before;
	cur -> Before = Last->Before;
	cur->After = Last;
	cur->Before ->After = cur->After->Before = cur;
}
