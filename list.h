#ifndef __LIST_H__
#define __LIST_H__

#include "listnode.h"

class List//注意插入不影响list队列 
{
public:
    static listnode* First;
	static listnode* Last;
	static UC buffer[pageLength];
	static char dataFolder[10];
	static int sizeLimit;
	
	
//	list();//初始化时候请加入哨兵 

	
	static void shift(listnode* cur);
	static void insert(listnode * cur);
	static void remove(listnode * cur);
	static void clean();
	static void init();
    static int NowTotal;//记录当前有多少页存在缓存中
    static listnode* renew(int T,page*,int numS,int strS ,int rowNeeded);//读取第T页的内容并更新list，即将此页插入到队列尾，如果队列太大则将队列头删除并释放空间
    //请注意我们的格式一堆4字节的数字然后是每个str后面跟着一个逗号，这个过程由于反复调用请务加速！！！！
    //应该只有这一种方式插入 
    //突然发觉好像只需要在删除的时候更新某一页，要不然不用立刻存进去，这个策略特别对于虚拟中间结果页有用 
};

#endif
