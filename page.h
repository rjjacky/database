#ifndef __PAGE_H__
#define __PAGE_H__

#include "listnode.h"



class table;
class page//每一页用一个数字Text表示，既是编号也是在OS中存储的文件名称 
{
public:
static int NowTotP;//目前总页数   
static UC retbuffer[pageLength],strbuffer[pageLength],tmpbuffer[pageLength];
    
    page* Before;
	page* After;//页采用链表形式存放

	table* parent;
	int rowS;
	int len;
	//writeToDist 的时候更新

    int Text; //编号 
    listnode* ListPos;//指向缓存区，如果是Null则表示此页没有存在缓存中需要读取
	//TODO:	这里需要用到list....暂时未实现
    listnode* getPage();//如果有则直接给出，否则执行listnode中的过程并把listnode返回出来 
    void ReWritePage(listnode*);//如果有插入则结果将是一个listnode，然后请你更新这个页，
    //注意此操作可能会影响部分在list中的页所以请同时判断是否在list中存在如果存在请更新并释放空间 
    void getCSV(const vector<string>& ColUsed,vector<string>& result);//用ColUsed的顺序将答案输出来 

	page(table* parent ,int Text ,page* pre,page* next,vector<string>& rows);
	page(table* parent,int Text,page* pre,page* next);
	~page();
	//将b附加在a的后面
	void add(UC*& a,UC*& b,int k = 1);
	//将数字b附加在a后面
	//
	void add(UC*& a,int b);
	void add(UC*& a,const string& s);
	
	void writeToDisk(UC* buffer,size_t lenToWrite,bool toAppend = false);
	void writeToDisk(listnode* ln, vector<bool>& colType);
	int parse(vector<string>& rows);
	int parse(listnode* ln,vector<bool> & colType);
	void insert(const string& st);
	int getLastRow();

	void print(){
		getPage()->print();
	}


};

#endif
