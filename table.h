#ifndef __TABLE_H__
#define __TABLE_H__


#include "page.h"
#include "listnode.h"
#include "judge.h"

class db;
class table
{
public:
	static int tableCount;
	table(int numS ,int strS,int maxRowLength,db* parent ,const vector<bool>& ColType, const vector<string>& ColName);
	~table();
	vector<bool> ColType;
    vector<string> ColName;//存储关于列的名字，便于开始插入时使用 

	page* getNewPage();
    void IntoMess(const vector<string>& row);//对于初始输入时候的插入，用这种简单的方式即可 ,Notice仅用于开始时候的插入哦！ 
    //不知道具体的格式CSV？注意看一下他的程序示例吧 
    void IntoOne(const string& st);//对于中间状态的插入，注意常数优化, st是用CSV格式表示即xx,xx,xx 
    void insertMessage(listnode*);//将某一页内容插进去，注意插入?不影响list队列???? 
    void renewPage(listnode*,page*);//将某一页内容更新进去 
    
    void reDb(vector<Judge*>&JudgeUsed);//非常特殊的一个函数，请将所有ColName中的元素在MyDb中的Map的值全部都还原回来 
    //顺便把JudgeUsed里面的T值全部都更新一遍 
    listnode* getLastPage(int& lastRow);//得到最后一页，注意请留下至少一hang是空的！！！！！！！！如果少了注意加入新的页哦 
//    int getLastRow();//得到最后一列的下一列在最后一页的位置 , 注意这个时候可以将TotR加了！ 
    
	page* head;
	page* tail;
    int numS,strS,TotR;//数字变量数，str变量数，并且记得维护总行数以供决策，存储的方式是先数字再字符串 ,最后还有每页最大容量数 
	int rowLength; //maxLength for per row
	int rowPerPage;

	db* parent;
	page* getFirstPage(){
		return head->After;
	}
	void print(){
		cout<<TotR<<endl;
		foreach(e,ColName,vector<string>){
			cout<<*e<<" ";
		}cout<<endl;

		page* cur = head->After;
		while (cur!=tail){
			cur->print();
			cur=cur->After;
		}
	}
};

#endif
