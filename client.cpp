﻿#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <map>

#include "../include/client.h"
#include "db.h"
#include "list.h"
#include "hashForS.h"
#include "hashForI.h"
//#include "tools.h"

template<typename T>
void showVector(T& a){
	for(int i = 0;i<a.size();i++){
		//cout<<a[i]<<endl;
	}
}

db MyDb;
hashForI HashI;
hashForS HashS;// 为了加快速度只好这么加速了 
int SizeOfP=32000;//请回答一次装多少页？ 
//int SizeOfH=4999997;
vector<string>ColUsed;
table *Ans;//Ans就是你要输出的那个table 
page*NowA; 

//	getTableByName对应的判断需要修改一下，现在改成了0对应int,其他数字为varchar的长度，因为我需要用到长度
//	给你调整了缩进，发现union的括号匹配有问题，下面有标注
//
//
//
//
//
//
//


void create(const string& table, const vector<string>& column,
		const vector<string>& type, const vector<string>& key)
{
	////cout<<"create"<<endl;
	for (int i = 0;i<column.size();i++){
	//	//cout<<column[i]<<endl;
	}

	MyDb.InsertT(table,column,type);
	////cout<<"/create"<<endl;

}

void train(const vector<string>& query, const vector<double>& weight)
{
	/*
	   HashI.H=new hashpointI*[SizeOfH];
	   for (int i=0;i<SizeOfH;i++) HashI.H[i]=NULL;
	   HashS.H=new hashpointS*[SizeOfH];
	   for (int i=0;i<SizeOfH;i++) HashS.H[i]=NULL;
	   */


	//只想到一个有用的，如果train某个部分比较多则先放进我们的List里面以节约第一次读入的时间 
	//I don't need such a stupid func
}

void load(const string& table, const vector<string>& row)
{
	////cout<<"<load>"<<endl;

	int l = row.size();
	MyDb.getTableByName(table)->IntoMess(row);
	////cout<<"</load>"<<endl;
}

void preprocess()
{
	List::init();
	//I don't need such a stupid func
}
// 自动缩进把selfchoose缩到Union内部了，表明缩进有问题，
table* Union(int flag,vector<Judge*>&JudgeUsed,vector<string>ColUsed)
{
	bool *t1I,*t1S,*t2I,*t2S;
	table *t1=JudgeUsed[flag]->t1,*t2=JudgeUsed[flag]->t2;
	if (t1->TotR>t2->TotR) 
	
	{
		////cout<<"table swaped"<<endl;
		swap(JudgeUsed[flag]->t1,JudgeUsed[flag]->t2);
		swap(JudgeUsed[flag]->s1,JudgeUsed[flag]->s2);
	}
	t1 = JudgeUsed[flag]->t1;
	t2 = JudgeUsed[flag]->t2;

	//t1作为小头在内循环，t2在外循环 
	////cout<<"flag "<<flag<<endl;
	////cout<<"judge  "<<JudgeUsed[flag]->s1<<" "<<JudgeUsed[flag]->s2<<endl;
	bool Tp=MyDb.getColumnType(JudgeUsed[flag]->s1);
	int t1Pos=MyDb.getColumnNumber(JudgeUsed[flag]->s1);
	int t2Pos=MyDb.getColumnNumber(JudgeUsed[flag]->s2);

	////cout<<"t2Pos "<<t2Pos<< "   "<<JudgeUsed[flag]->s2<<endl;

	vector<int> Data1,Data2,Str1,Str2,DataPos1,DataPos2,StrPos1,StrPos2;//第几个判断用到了并且加上具体在页中的位置 
	Data1.clear();Data2.clear();Str1.clear();Str2.clear();
	DataPos1.clear();DataPos2.clear();StrPos1.clear();StrPos2.clear();
	for (int i=0;i<JudgeUsed.size();i++)
		if (JudgeUsed[i]->Type!=3)
			if (JudgeUsed[i]->Type!=2)
			{
				if (JudgeUsed[i]->t1==t1) Data1.push_back(i),DataPos1.push_back(MyDb.getColumnNumber(JudgeUsed[i]->s1));
				else if(JudgeUsed[i]->t1==t2)   Data2.push_back(i),DataPos2.push_back(MyDb.getColumnNumber(JudgeUsed[i]->s1));
			} else
				if (JudgeUsed[i]->t1==t1) Str1.push_back(i),StrPos1.push_back(MyDb.getColumnNumber(JudgeUsed[i]->s1));
				else if(JudgeUsed[i]->t1 == t2)   Str2.push_back(i),StrPos2.push_back(MyDb.getColumnNumber(JudgeUsed[i]->s1));

	t1I=new bool[t1->numS];
	t1S=new bool[t1->strS];
	t2I=new bool[t2->numS];
	t2S=new bool[t2->strS];
	memset(t1I,0,sizeof(bool)*t1->numS);
	memset(t1S,0,sizeof(bool)*t1->strS);
	memset(t2I,0,sizeof(bool)*t2->numS);
	memset(t2S,0,sizeof(bool)*t2->strS);
	vector<string> NewList;
	NewList.clear();
	for (int i=0;i<ColUsed.size();i++)
		if (MyDb.getTableByColumn(ColUsed[i])==t1) 
		{
			if (!MyDb.getColumnType(ColUsed[i])) t1I[MyDb.getColumnNumber(ColUsed[i])]=true;
			else                    t1S[MyDb.getColumnNumber(ColUsed[i])]=true;
			//NewList.push_back(ColUsed[i]);
		}
		else
			if (MyDb.getTableByColumn(ColUsed[i])==t2)
			{
				if (!MyDb.getColumnType(ColUsed[i])) t2I[MyDb.getColumnNumber(ColUsed[i])]=true;
				else                    t2S[MyDb.getColumnNumber(ColUsed[i])]=true;
				//NewList.push_back(ColUsed[i]);
			}
	for (int i=0;i<JudgeUsed.size();i++)
		if (i!=flag)
			if (JudgeUsed[i]->Type==3)
			{
				string st=JudgeUsed[i]->s1;
				
				if (MyDb.getTableByColumn(st)==t1)
				{
					if (MyDb.getColumnType(st)==0) t1I[MyDb.getColumnNumber(st)]=true;
					else                  t1S[MyDb.getColumnNumber(st)]=true;
					//NewList.push_back(st);
				}
				if (MyDb.getTableByColumn(st)==t2)
				{
					if (MyDb.getColumnType(st)==0) t2I[MyDb.getColumnNumber(st)]=true;
					else                  t2S[MyDb.getColumnNumber(st)]=true;
					//NewList.push_back(st);
				}
				st=JudgeUsed[i]->s2;
				if (MyDb.getTableByColumn(st)==t1)
				{
					if (MyDb.getColumnType(st)==0) t1I[MyDb.getColumnNumber(st)]=true;
					else                  t1S[MyDb.getColumnNumber(st)]=true;
					//NewList.push_back(st);
				}
				if (MyDb.getTableByColumn(st)==t2)
				{
					if (MyDb.getColumnType(st)==0) t2I[MyDb.getColumnNumber(st)]=true;
					else                  t2S[MyDb.getColumnNumber(st)]=true;
					//NewList.push_back(st);
				}
			}

	for (int i=0;i<t1->numS+t1->strS;i++)
		if (MyDb.getColumnType(t1->ColName[i])==0)
			if (t1I[MyDb.getColumnNumber(t1->ColName[i])]) NewList.push_back(t1->ColName[i]);
		for (int i=0;i<t1->numS+t1->strS;i++)
		if (MyDb.getColumnType(t1->ColName[i])!=0)
			if (t1S[MyDb.getColumnNumber(t1->ColName[i])]) NewList.push_back(t1->ColName[i]);
		for (int i=0;i<t2->numS+t2->strS;i++)
		if (MyDb.getColumnType(t2->ColName[i])==0)
			if (t2I[MyDb.getColumnNumber(t2->ColName[i])]) NewList.push_back(t2->ColName[i]);
		for (int i=0;i<t2->numS+t2->strS;i++)
		if (MyDb.getColumnType(t2->ColName[i])!=0)
			if (t2S[MyDb.getColumnNumber(t2->ColName[i])]) NewList.push_back(t2->ColName[i]);
		
	string NewTName=MyDb.newT(NewList);
	table *NewT=MyDb.getTableByName(NewTName);     
	int D1J=Data1.size(),D2J=Data2.size(),S1J=Str1.size(),S2J=Str2.size();  
//	//cout<<"J!  "<<D1J<<" "<<D2J<<" "<<S1J<<" "<<S2J<<endl;

	page* p=t1->getFirstPage();
	//selfchoose 上的那一个括号匹配到了这里
	page* tail = t1->tail;
	if (Tp==false)//int	
	{
		while (p!=tail)
		{
			for (int i=0;i<SizeOfP && p!=tail;i++,p=p->After)
			{
				listnode* Now=p->getPage();
				int pj = p->getLastRow();
				Now->updated = true;
				Now->rowS = pj;
				for (int j=0;j<pj;j++)
				{
					bool JC=true;
					for (int k=0;k<D1J;k++){
						if (JudgeUsed[Data1[k]]->Type==-1)
						{
							if (Now->I[DataPos1[k]][j]>=JudgeUsed[Data1[k]]->s1_I) {JC=false;break;}
						} else
							if (JudgeUsed[Data1[k]]->Type==1)
							{
								if (Now->I[DataPos1[k]][j]<=JudgeUsed[Data1[k]]->s1_I){JC=false;break;}
							} else
								if (Now->I[DataPos1[k]][j]!=JudgeUsed[Data1[k]]->s1_I){JC=false;break;}
					}
					if (JC){
						for (int k=0;k<S1J;k++)
							if (Now->S[StrPos1[k]][j]!=JudgeUsed[Str1[k]]->s2){JC=false;break;}
					}
					else{
						////cout<<" JC   被够吃了 2 "<<endl;
					}

					if (JC) HashI.insertinto(p,j,t1Pos);
				}
			} 
			page* qPage=t2->getFirstPage();
			page* t2tail = t2->tail;
			while (qPage!=t2tail)
			{
				////cout<<"before getPage"<<endl;
				listnode *q=qPage->getPage();
				////cout<<"after getPage"<<endl;
				int pj =qPage->getLastRow();
				q->updated = true;
				q->rowS = pj;
				for (int j=0;j<pj;j++)
				{
					bool JC=true;
					for (int k=0;k<D2J;k++)
						if (JudgeUsed[Data2[k]]->Type==-1)
						{
							if (q->I[DataPos2[k]][j]>=JudgeUsed[Data2[k]]->s1_I) {JC=false;break;}
						} else
							if (JudgeUsed[Data2[k]]->Type==1)
							{
								if (q->I[DataPos2[k]][j]<=JudgeUsed[Data2[k]]->s1_I){JC=false;break;}
							} else
								if (q->I[DataPos2[k]][j]!=JudgeUsed[Data2[k]]->s1_I){JC=false;break;}
					if (JC){
						for (int k=0;k<S2J;k++){
							////cout<<"S2J  "<<StrPos2[k]<<"  "<<Str2[k]<<" "<<q->S[StrPos2[k]][j]<<"   "<<JudgeUsed[Str2[k]]->s2<<endl;

							if (q->S[StrPos2[k]][j]!=JudgeUsed[Str2[k]]->s2){JC=false;break;}
						}
					}
					else{
						////cout<<"JC bei gou chi le"<<endl;
					}


					if (JC) 
					{
						////cout<<"xixi"<<endl;
						////cout<<t2Pos<<"  "<<j<< " _______"<<q->I[t2Pos][j]<<endl;
						hashpointI *w=HashI.getHashp(q->I[t2Pos][j]);
						////cout<<w<<endl;

						while (w!=NULL)
						{
							////cout<<"ok at "<<w->H<<endl;
							int Pos;
							listnode *p1=w->P->getPage();
							listnode *IT=NewT->getLastPage(Pos);
							q = qPage->getPage();

							////cout<<w << " "<<IT<<"  "<<Pos<<endl;

							int l=0;
							////cout<<"*** "<<t1->numS<<endl;
							////cout<<"update "<<Pos<<" "<<w->Pos<<endl;
							//怎么能两个表都用w->pos呢？
							for (int k=0;k<t1->numS;k++){
								
								if (t1I[k]){
									IT->I[l++][Pos]=p1->I[k][w->Pos];
								}

							}
							////cout<<"____\n";
							for (int k=0;k<t2->numS;k++){
								////cout<<k<<" ";
								if (t2I[k]) IT->I[l++][Pos]=q->I[k][j];
							}////cout<<"+++\n";
							l=0;
							for (int k=0;k<t1->strS;k++)
								if (t1S[k]) IT->S[l++][Pos]=p1->S[k][w->Pos];
							for (int k=0;k<t2->strS;k++)
								if (t2S[k]) IT->S[l++][Pos]=q->S[k][j];
							w=w->Same;
						}
						////cout<<"out w"<<endl;
					}
					else{
						////cout<<"JC  bei gou chi le~~~"<<endl;
					}
				} 
				qPage=qPage->After;
			}
			HashI.clear();
		}
	}
	else//str 
	{
		////cout<<"str Union"<<endl;
		
		while (p!=tail)
		{
			for (int i=0;i<SizeOfP && p!=tail;i++,p=p->After)
			{
				////cout<<"Now getpage"<<endl;
				listnode* Now=p->getPage();
				int pj = p->getLastRow();
				Now->updated = true;
				Now->rowS = pj;
				for (int j=0;j<pj;j++)
				{
					bool JC=true;
					for (int k=0;k<D1J;k++)
						if (JudgeUsed[Data1[k]]->Type==-1)
						{
							if (Now->I[DataPos1[k]][j]>=JudgeUsed[Data1[k]]->s1_I) {JC=false;break;}
						} else
							if (JudgeUsed[Data1[k]]->Type==1)
							{
								if (Now->I[DataPos1[k]][j]<=JudgeUsed[Data1[k]]->s1_I){JC=false;break;}
							} else
								if (Now->I[DataPos1[k]][j]!=JudgeUsed[Data1[k]]->s1_I){JC=false;break;}
					if (JC)
						for (int k=0;k<S1J;k++)
							if (Now->S[StrPos1[k]][j]!=JudgeUsed[Str1[k]]->s2){JC=false;break;}

					if (JC) HashS.insertinto(p,j,t1Pos);
					else {
						//cout<<"JC B G C L\n";
					}

				}
			} 
			page* qPage=t2->getFirstPage();
			page* t2Tail = t2->tail;
			
			while (qPage!=t2Tail)
			{
				//cout<<"qPage getPage"<<endl;
				listnode *q=qPage->getPage();
				int pj = qPage->getLastRow();
				q->rowS = pj;
				q->updated = true;
				for (int j=0;j<pj;j++)
				{
					bool JC=true;
					for (int k=0;k<D2J;k++)
						if (JudgeUsed[Data2[k]]->Type==-1)
						{
							if (q->I[DataPos2[k]][j]>=JudgeUsed[Data2[k]]->s1_I) {JC=false;break;}
						} else
							if (JudgeUsed[Data2[k]]->Type==1)
							{
								if (q->I[DataPos2[k]][j]<=JudgeUsed[Data2[k]]->s1_I){JC=false;break;}
							} else
								if (q->I[DataPos2[k]][j]!=JudgeUsed[Data2[k]]->s1_I){JC=false;break;}
					if (JC){
						for (int k=0;k<S2J;k++)
							if (q->S[StrPos2[k]][j]!=JudgeUsed[Str2[k]]->s2){JC=false;break;}
					}
					if (JC) 
					{
						//cout<<"q->S[t2Pos][j] = "<<q->S[t2Pos][j]<<endl;
						hashpointS *w=HashS.getHashp(q->S[t2Pos][j]);
						//cout<<w<<endl;
						while (w!=NULL)
						{
							int Pos;
							listnode *p1=w->P->getPage();
							listnode *IT=NewT->getLastPage(Pos);
							q = qPage->getPage();

							int l=0;
							for (int k=0;k<t1->numS;k++)
								if (t1I[k]) IT->I[l++][Pos]=p1->I[k][w->Pos];
							for (int k=0;k<t2->numS;k++)
								if (t2I[k]) IT->I[l++][Pos]=q->I[k][j];
							l=0;
							for (int k=0;k<t1->strS;k++)
								if (t1S[k]) IT->S[l++][Pos]=p1->S[k][w->Pos];
							for (int k=0;k<t2->strS;k++)
								if (t2S[k]) IT->S[l++][Pos]=q->S[k][j];
							w=w->Same;
						}
					}
					else{
						//cout<<"JC Bei gou chi liao \n";
					}
				} 
				qPage=qPage->After;
			}
			HashI.clear();
		}
	}

	//cout<<"now is new table"<<endl;
	int delP=0;
	//erase的效率问题？
	JudgeUsed.erase(JudgeUsed.begin()+flag);

	while (delP<JudgeUsed.size())
		if (JudgeUsed[delP]->Type!=3)
			if (JudgeUsed[delP]->t1==t1 || JudgeUsed[delP]->t1==t2) JudgeUsed.erase(JudgeUsed.begin()+delP);
			else delP++;
		else delP++;
	NewT->reDb(JudgeUsed);


	delete[] t1I;
	delete[] t2I;
	delete[] t1S;
	delete[] t2S;
	return NewT;

}
//我怀疑是这里少了一个括号 ， 加上之后就能够正常缩进了，但是return 语句被放进了第二层括号，说明倒数第二个括号的位置不对，你调整一下吧，再加上union里面的new对应的delete操作
table* SelfChoose(table* t1,vector<Judge*>&JudgeUsed,const vector<string>& ColUsed)
{
	////cout<<"selfchoose"<<" "<<JudgeUsed.size()<<endl;
	vector<string> NewList;
	NewList.clear();
	bool *t1I,*t1S;
	t1I=new bool[t1->numS];
	t1S=new bool[t1->strS];
	memset(t1I,0,sizeof(bool)*t1->numS);
	memset(t1S,0,sizeof(bool)*t1->strS);
	////cout<<"check -1"<<endl;

	for (int i=0;i<ColUsed.size();i++){
		////cout<<MyDb.getColumnType(ColUsed[i])<<" "<<MyDb.getColumnNumber(ColUsed[i])<<endl;

		if (!MyDb.getColumnType(ColUsed[i])) t1I[MyDb.getColumnNumber(ColUsed[i])]=true;
		else                    t1S[MyDb.getColumnNumber(ColUsed[i])]=true;
	}



	int len = t1->numS + t1->strS;
	for (int i = 0 ;i < len; i++){
		////cout<<t1->ColType[i]<<endl;
		if (t1->ColType[i]){
			if (t1S[MyDb.getColumnNumber(t1->ColName[i])])
				NewList.push_back(t1->ColName[i]);
		}
		else{
			if (t1I[MyDb.getColumnNumber(t1->ColName[i])])
				NewList.push_back(t1->ColName[i]);
		}
	}
	////cout<<"check 0"<<endl;
	////cout<<NewList.size()<<endl;

	string NewTName=MyDb.newT(NewList);
	table *NewT=MyDb.getTableByName(NewTName);

	page* p=t1->getFirstPage();

	////cout<<"check 0.5"<<endl;
	int DJ=JudgeUsed.size();
	int *JPos=new int[JudgeUsed.size()];
	for (int i=0;i<DJ;i++) JPos[i]=MyDb.getColumnNumber(JudgeUsed[i]->s1);
	//cout<<"check 1"<<endl;
	page* tail = t1->tail;
	while (p!=tail)
	{
		listnode* Now=p->getPage();
		//cout<<"now "<<Now<<endl;
		int pj = p->getLastRow();
		//cout<<"pj "<<pj<<endl;
		Now->rowS = pj;
		Now->updated = true;
		for (int j=0;j<pj;j++)
		{
			//cout<<"j = "<<j<<endl;
			bool JC=true;
			for (int k=0;k<DJ;k++){
				//cout<<"judge "<<JudgeUsed[k]->Type<<endl;
				if(j==10){
					//cout<<"*** "<<JudgeUsed[k]->s2<<" "<<Now->S[JPos[k]][j]<<endl;
				}
				if (JudgeUsed[k]->Type==2)
				{
					if (JudgeUsed[k]->s2!=Now->S[JPos[k]][j]) {JC=false;break;}
				} else
					if (JudgeUsed[k]->Type==1)
					{
						if (Now->I[JPos[k]][j]<=JudgeUsed[k]->s1_I){JC=false;break;}
					} else
						if (JudgeUsed[k]->Type==0)
						{
							if (Now->I[JPos[k]][j]!=JudgeUsed[k]->s1_I){JC=false;break;}
						} else
							if (JudgeUsed[k]->Type==-1)
								if (Now->I[JPos[k]][j]>=JudgeUsed[k]->s1_I){JC=false;break;}
			}
			if (JC)
			{
				//Modified
				int Pos;
				listnode *IT=NewT->getLastPage(Pos);
				//cout<<"check 1.5"<<endl;
				int l=0;
				for (int k=0;k<t1->numS;k++)
					if (t1I[k]) IT->I[l++][Pos]=Now->I[k][j];
				l=0;
				//cout<<"check 1.55"<<endl;
				for (int k=0;k<t1->strS;k++){
					if (t1S[k]) IT->S[l++][Pos]=Now->S[k][j];
				}
			}
		}

		//cout<<"check 1.6"<<endl;
		p=p->After;
	}

	//cout<<"check 2"<<endl;
	JudgeUsed.clear();
	NewT->reDb(JudgeUsed);
	//cout<<"check 3"<<endl;
	delete[] t1S;
	delete[] t1I;
	delete[] JPos;

	return NewT;
}

void splitSQL(string s,vector<string>&ColUsed,vector<string>&fromUsed,vector<string>&whereUsed)
{
	int i=7;
	while (s[i]!='F' || s[i+1]!='R' || s[i+2]!='O' || s[i+3]!='M')
	{
		string td="";
		while (s[i]==' ' || s[i]==',') i++;
		while (s[i]!=' ' && s[i]!=',') td+=s[i++];
		while (s[i]==' ' || s[i]==',') i++;
		ColUsed.push_back(td);
	}
	i+=4;
	while (true)
	{
		string td="";
		while (s[i]==' ' || s[i]==',') i++;
		while (s[i]!=' ' && s[i]!=',') td+=s[i++];
		fromUsed.push_back(td);
		if (s[i]==';') break;
		while (s[i]==' ' || s[i]==',') i++;
		if (s[i]==';') break;
		if (s[i]=='W' && s[i+1]=='H' && s[i+2]=='E' && s[i+3]=='R' && s[i+4]=='E') break;
	}
	if (s[i]==';') return;
	i+=5;
	while (true)
	{
		string td="";
		while (s[i]==' ' || s[i]==',') i++;
		while (s[i]!=' ' && s[i]!=',') td+=s[i++];
		whereUsed.push_back(td);
		if (s[i]==';') break;
		while (s[i]==' ' || s[i]==',') i++;
		if (s[i]==';') break;
	}
	/*
	cout<<"S"<<endl;
	foreach(e,ColUsed,vector<string>){
		cout<<*e<<" ";
	}cout<<endl<<"F";
	foreach(e,fromUsed,vector<string>){
		cout<<*e<<" ";
	}cout<<endl<<"W";
	foreach(e,whereUsed,vector<string>){
		cout<<*e<<" ";
	}cout<<endl;
	*/
}

void ques(string sql)
{
	ColUsed.clear();
	vector<string> fromUsed;
	vector<string> whereUsed;
	
	splitSQL(sql,ColUsed,fromUsed,whereUsed);
	//parseSQL(sql,ColUsed,fromUsed,whereUsed);

	//TODO: 在末尾添加字符？那么应该是用char(13)吧
	//string加法亦然很慢，建议改成char 数组，最后再一步赋值为string即可

	vector<table*> TabUsed;
	TabUsed.clear();
	foreach(e,fromUsed,vector<string>)
		TabUsed.push_back(MyDb.getTableByName(*e));

	vector<Judge*> JudgeUsed;
	JudgeUsed.clear(); 
	for(int i = 0;i<whereUsed.size();i+=4){
		Judge *p=new Judge();
		string& s1 = whereUsed[i];

		p->s1 = s1;
		p->s1_T=MyDb.getColumnType(s1);
		p->s1_I=MyDb.getColumnNumber(s1);
		p->t1=MyDb.getTableByColumn(s1);

		char c = whereUsed[i+1][0];
		string &s = whereUsed[i+2];
		int tmp = 0;
		if(s[0]==39){
			p->s2 = s.substr(1,s.length()-2);
			p->Type = 2;
			//cout<<"Type 2 "<< MyDb.getTableByName("customer")<<"  "<<p->t1<<endl;
		}
		else if ('0'<=s[0]&&s[0]<='9'){
			tmp =0;
			for (int k = 0;k<s.length();k++){
				tmp = tmp*10+s[k]-'0';
			}

			if (c=='<') p->Type=-1;
			if (c=='>') p->Type=1;
			if (c=='=') p->Type=0;
			p->s1_I = tmp;
		}
		else{
			p->s2 = s;
			p->Type = 3;
			p->s2_T=MyDb.getColumnType(s);
			p->s2_I=MyDb.getColumnNumber(s);
			p->t2=MyDb.getTableByColumn(s);

		}
		JudgeUsed.push_back(p);
	}
	//cout<<"judgeUsed.size() "<<JudgeUsed.size()<<endl;
	//未完待续，接下来请你每次选两个最小的然后合并，并将每次生成的结果放入一个table*中 
	vector<Judge*> backupJudgeUsed = JudgeUsed;
	
	Ans=NULL;
	if (JudgeUsed.size())
		while (JudgeUsed.size())
		{
			int flag=-1,best=2147483640;
			int Size=JudgeUsed.size();
			for (int i=0;i<Size;i++)
				if (JudgeUsed[i]->Type==3)
					if (JudgeUsed[i]->t1->TotR*JudgeUsed[i]->t2->TotR<best)
						best=JudgeUsed[i]->t1->TotR*JudgeUsed[i]->t2->TotR,flag=i;
			if (flag>=0) Ans=Union(flag,JudgeUsed,ColUsed);//记得新出结果之后要将原来部分也要合并，即改t1,t2值 
			else     Ans=SelfChoose(JudgeUsed[0]->t1,JudgeUsed,ColUsed);
		}
	else Ans=SelfChoose(MyDb.getTableByColumn(ColUsed[0]),JudgeUsed,ColUsed);
	//cout<<"Ans "<<Ans<<endl;
	for(int i = 0;i<Ans->ColName.size();i++){
		//cout<<Ans->ColName[i]<<endl;
	}
	NowA=Ans->getFirstPage();
	for(int i = 0;i<backupJudgeUsed.size();i++){
		delete backupJudgeUsed[i];
	}
} 

//按照我的接口改一下吧
vector<string> result;
int RemainRow;
int next(char *row)
{
	page* tail = NowA->parent->tail;
	if (RemainRow<result.size()) {strcpy(row,result[RemainRow++].c_str());return 1;}
	else
		if (NowA!=tail)
		{
			result.clear();
			NowA->getCSV(ColUsed,result);
			if(result.size()==0){
				MyDb.FixDb();
				return 0;
			}
			RemainRow=1;
			NowA=NowA->After;
			strcpy(row,result[0].c_str());
			return 1;
		} else 
		{
			MyDb.FixDb();
			return 0;
		}
}     
void inse(string s)
{
	int p=12;
	string st="";
	while (s[p]!=' ') st+=s[p++];
	//TODO: string 加法的效率？
	table *T=MyDb.getTableByName(st);
	int Len=s.length();
	while (p<Len)
	{
		st="";
		while (s[p]!='(' && p<Len) p++;
		if (p==Len) break;
		p+=2;
		while (s[p]!=' ') st+=s[p++];
		T->IntoOne(st);
	}
} 


void execute(const string& sql)
{
	//cout<<"execute"<<endl;
	//cout<<sql<<endl;
	if (sql[0]=='S') ques(sql);
	else          inse(sql);
	//cout<<"/execute"<<endl;
}

void close(){

}
