
#include "table.h"
#include "list.h"
#include "judge.h"
#include "db.h"

int table::tableCount = 0;

table::table(int nS,int sS,int rL,db* p, const vector<bool>& CT, const vector<string>& CN){
	numS = nS;
	strS = sS;
	
	TotR = 0;
	rowLength = rL;
	rowPerPage = pageLength/rowLength;
	ColType = CT;
	ColName = CN;
	parent = p;
	head = new page(this,-1,NULL,NULL);
	tail = new page(this,-2,head,NULL);
	head->After = tail;
	
}

void table::IntoMess(const vector<string>& row){
	
	int l = row.size();
	TotR += l;	
	int k = rowPerPage;
	int p = l/k;
	page* newPage;
	vector<string> tmp;
	for (int i = 0 ;i < p; i++){
		for (int j = i*k;j<(i+1)*k;j++){
			tmp.push_back(row[j]);
		}
		newPage = new page(this,page::NowTotP++,tail->Before,tail,tmp);
		//cout<<"page "<<newPage<<" size "<<newPage->rowS<<"  "<<newPage->ListPos<<endl;
		////cout<<"new "<<newPage<<endl;
		tail->Before->After = newPage;
		tail->Before = newPage;
		tmp.clear();
	}
	for (int j = p*k;j<l;j++){
		tmp.push_back(row[j]);
	}
	if(tmp.size()){
		newPage = new page(this,page::NowTotP++,tail->Before,tail,tmp);
		////cout<<"new "<<newPage<<endl;
		tail->Before = tail->Before->After = newPage;
		
	}
}

table::~table(){

	page* cur = tail;
	while(cur!=head){
		page* b = cur->Before;
		delete cur;
		cur = b;
	}
	delete cur;
}

void table::IntoOne(const string& st){

	TotR ++ ;
	
	page* cur = head->After;
	while(cur!=tail){
		if(cur->ListPos!=NULL){
			if (cur->ListPos->rowS < rowPerPage ) {
				cur->ListPos->insert(st);
				//TOIGNORE://TODO : count this, if this is trival, drop vector in listnode
				return;
			}
		}
		cur = cur->After;
	}
	cur = head->After;
	////cout<<"cur "<<cur<<endl;
	//
	//TODO : 如果这种以固定最大行数的做法有太大的空间浪费，就修改成动态的

	while(cur!=tail){
		////cout<<"* "<<cur<<" "<<cur->ListPos<<endl;
		if(cur->ListPos==NULL){
			////cout<<cur<<endl;
			if (cur->rowS < rowPerPage){
				cur->insert(st);
				return;
			}
		}
		cur = cur->After;
	}
	page* npg = getNewPage();
	npg->ListPos->insert(st);
}
	
		





void table::insertMessage(listnode* ln ){
	listnode* l = List::Last;
	listnode* p = l->Before;
	p->After = l->Before = ln;
	ln->Before = p;
	ln->After = l;
}


void table::renewPage(listnode* ln, page* pg){
	if(ln->updated){
		pg->rowS = ln->rowS;
		pg->writeToDisk(ln,ColType);
	}


}

void table::reDb(vector<Judge*>& JudgeUsed){
	//cout<<"reDb"<<endl;
	Judge* cur;
	int l = ColName.size();

	int iN = 0,sN = 0;
	for(int i = 0 ;i <l ;i ++){
		if(ColType[i]){
			parent->ColumnNumMap[ColName[i]] = sN++;
		}
		else{
			parent->ColumnNumMap[ColName[i]] = iN++;
		}
		parent->ColumnMap[ColName[i]] = this;
	}
	foreach(e,JudgeUsed,vector<Judge*>){
		cur = *e;
		if (cur->Type == 3){
			//cout<<"s2 = "<<cur->s2<<endl;
			//cout<<parent->getTableByColumn(cur->s2)<<endl;
			cur->t2 = parent->getTableByColumn(cur->s2);
		}
		cur->t1 = parent->getTableByColumn(cur->s1);
		//cout<<"s1 = "<<cur->s1<<endl;
	}

		

}
listnode* table::getLastPage(int& lastRow){
	
	//cout<<"getLastPage"<<endl;

	TotR++;
	//cout<<tail<<endl;
	//cout<<tail->Before<<endl;
	if (tail->Before->getLastRow() < rowPerPage){
		//cout<<"p1"<<endl;
		//cout<<tail->Before->ListPos<<endl;
		listnode* cur =  tail->Before->getPage();
		//cout<<"cp1"<<endl;
		lastRow = cur->LastRow();
		//cout<<"cp2"<<endl;
		cur->rowS++;
		return cur;
	}
	else{
		//cout<<"p2"<<endl;
		lastRow = 0;
		listnode* p = getNewPage()->ListPos;
		p->rowS = 1;
		return p;
		//TODO : return new Page

	}
	//cout<<"/getNewPage"<<endl;
}


page* table::getNewPage(){
	page* cur = new page(this,page::NowTotP++,tail->Before,tail);
	tail->Before->After = cur;
	tail->Before = cur;
	listnode* ln = new listnode(cur, NULL,NULL,numS,strS,rowPerPage);
	List::insert(ln);
	//因为这里没有把page写入磁盘,因此默认为updated
	ln->updated = true;
	cur->ListPos = ln;

	return cur;

}
		

