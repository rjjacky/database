#include "hashForS.h"

//TODO: 后面尝试用一下暴雪hash
int hashForS::bucketLength = 1313131;
hashForS::hashForS(){
	H = new hashpointS*[bucketLength];
	for (int i = 0 ;i <bucketLength;i++){
		H[i] = NULL;
	}
}
hashForS::~hashForS(){
	clear();
	delete[] H;
}
void hashForS::clear(){

	hashpointS *p1,*p2;
	hashpointS *n1,*n2;
	for(int i = 0;i<bucketLength;i++){
		p1 = H[i];
		while(p1){
			n1 = p1->Next;

			{
				p2 = p1->Same;
				while(p2){
					n2 = p2->Same;
					delete p2;
					p2 = n2;
				}
			}

			delete p1;
			p1 = n1;
		}
		H[i] = NULL;
	}

}
int hashForS::getH(const string& X){
	int seed = 131;
	int l = X.length();
	unsigned int hash = 0;
	for(int i = 0;i<l;i++){
		hash = hash*seed+X[i];
	}
	return hash&0x7fffffff;
}

void hashForS::insertinto(page* P,int X,int Y){
	listnode* node = P->getPage();
	//int value = getH(node->S[X][Y]);
	string value = node->S[Y][X];
	int hash = getH(value)%bucketLength;
	//cout<<"insertinto "<<value<<"  "<<hex<<hash<<endl;
	hashpointS* cur = new hashpointS(X,value,P);

	for(hashpointS* e = H[hash];e;e=e->Next){
		if (e->H == value){
			cur->Same = e->Same;
			e->Same = cur;
			return ;
		}
	}
	cur->Next = H[hash];
	H[hash] =cur;
}

hashpointS* hashForS::getHashp(const string& X){
	int hash = getH(X)%bucketLength;

	//cout<<"getHashStr  "<<X<< " "<<hex<<hash<<endl;

	for (hashpointS* e = H[hash];e;e=e->Next){
		if (e->H == X){
			return e;
		}
	}
	return NULL;
}
