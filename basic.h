#ifndef __BASIC_H__
#define __BASIC_H__

#include <iostream>
#include <vector>
#include <map>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
//#include "io.rj.h"
using namespace std;

const int pageLength = 8096;
typedef  char UC;


#define isValid(c) ((c==' ')||(c==',')||('0'<=c&&c<='9')||('a'<=c&&c<='z')||('A'<=c&&c<='Z'))
#define foreach(e,set,type) for (type::iterator e=(set).begin();e!=(set).end();++e)


#endif
