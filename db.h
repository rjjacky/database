#ifndef __DB_H__
#define __DB_H__

#include "list.h"
#include "table.h"

#define psi pair<string,int>
#define psb pair<string,bool>
#define pst pair<string,table*>

class db
{
public:
    db();//初始化都在里面完成
	~db();
	//现在正式确定nowT是临时表的计数器
static int NowT; 
    string newT(const vector<string> &ColList);//随机给一个名字,求给一个新Table的名字，用str表示这个新表的名字，随便取吧暂定用数字+1表示表的名字 
    //初始至少给1页吧！！！！！ 
    //注意这个时候暂时不要 更新Db里面的几个Map
     
	//list is now a static class
   // list *MyL;//此dbms对应的list 
   // 
   // if map is too slow , we can consider hashmap
    map<string,int> ColumnNumMap;//得到此列是table中的第几列
    map<string,int> ColumnType;// 更新为：0表示int，其他正数为varchar的长度//得到此列是什么类型，只有两种False为int否则Ture为varchar() 

    map<string,table*> ColumnMap;//得到此列属于哪个table 
    map<string,table*> TableMap;//得到此名字对应哪个table 
    table* getTableByName(string s);//通过名字获得table 
    table* getTableByColumn(const string& s);//通过名字获得column对应的table
    int getColumnType(string s);//通过名字获得column对应的在table中的属性 
    int getColumnNumber(const string& s);//通过名字获得cloumn对应的在table中属于第几列
    table* InsertT(const string& Name,const vector<string>& Col,const vector<string>& Type);//插入一个Table 
    
    
    vector<table*> OriginalTable;//将上面的InsertT的部分的Table存进来（即最开始时候的Table，不是中间Table） 
	
	vector<int> tempTable; // 记录每个fixdb周期内生成的临时表
    void FixDb();//利用上面那个Vector逐个运行reDb即可 

	
	
	void printMap(){
		cout<<"ColumnMap"<<endl;
		map<string,table*>::iterator iter = ColumnMap.begin();
		for(;iter!=ColumnMap.end();iter++){
			cout<<iter->first<<" "<<iter->second<<endl;
		}

	}
};
#endif
