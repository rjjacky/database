#ifndef __LISTNODE_H__
#define __LISTNODE_H__

#include "basic.h"
using namespace std;


class page;
class listnode//这个是一页的内容单位 
{
public:
	static char buffer[pageLength]; 
	//由于按列存储，因此第一维在一开始就固定了，所以可以尝试回到一开始的指针的做法，一开始把空间分配满
	string** S;
	int** I;
	//两大数据块，S[i][j]表示第i列第j行的数据值 
    
	listnode* Before;
	listnode* After;
    page* PagePos;//和上面的互指
	int rowS;
	int numS;
	int strS;
	int rowLimit;
	bool updated ;

    //NOTICE
    //NOTICE
	
	listnode( page* p ,UC* data = NULL,UC* end = NULL,int =0,int =0,int = 0);
	void parse(int ,int ,UC*,UC *end);
	void setPage(page* p);
	~listnode();

	void insert(const string& st);
	int LastRow();
	
	void print(){
		for (int i = 0;i < rowS;i++){
			for (int j =0 ;j < numS ;j++){
				cout<<I[j][i]<<" ";
			}
			for (int j = 0;j< strS;j++){
				cout<<S[j][i]<<" ";
			}
			cout<<endl;
		}
	}
};


#endif
